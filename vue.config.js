module.exports = {
    devServer: {
        port: 9090,
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '/handwritingAnalysisSys_Web_exploded'
                }
            }
        },
    },
}
