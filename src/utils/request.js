import axios from 'axios';
// 创建axios实例
const service = axios.create({
    // process.env.NODE_ENV === 'development' 来判断是否开发环境
    // easy-mock服务挂了，暂时不使用了
    // baseURL: 'https://www.easy-mock.com/mock/592501a391470c0ac1fab128',
    timeout: 50000
});
// request拦截器
service.interceptors.request.use(
    config => {
        return config;
    },
    error => {
        console.log(error);
        return Promise.reject();
    }
);
// response拦截器
service.interceptors.response.use(
    response => {
        const res = response.data;
        const headers = response.headers;

        if (res.code === 20000 || res instanceof Blob) {
            console.log(res)
            return res;
        } else if (headers['content-type'] === 'application/octet-stream;charset=utf-8') {
            return res;
        } else {
            return Promise.reject(res)
        }
    },
    error => {
        console.log('ERROR: ' + error);
        return Promise.reject(error);
    }
);

export default service;
