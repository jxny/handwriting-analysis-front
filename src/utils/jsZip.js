import data2blob from "vue-image-crop-upload/utils/data2blob";
import {saveAs} from 'FileSaver'
export default function (urls, mime,name) {
    // 使用 require 引入 jszip
    let JSZip = require("jszip");

    // 引入另一个 file-saver 库，用来把压缩好的文件保存到本地
    let zip = new JSZip();

    for (let i = 0; i < urls.length; i++) {
        zip.file(name+ (i+1) + ".docx",data2blob(urls[i], mime) , {base64: true});
    }
    zip.generateAsync({type: "blob"}).then(function (content) {
        // see FileSaver.js
        saveAs(content, `性格报告.zip`);//压缩后的zip名
    });
};

