import {createApp} from 'vue'
import App from './App.vue'
import router from './router/index.js'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import './assets/css/icon.css'
createApp(App).use(ElementPlus).use(store).use(router).mount('#app')