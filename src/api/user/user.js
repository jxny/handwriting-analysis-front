import request from "../../utils/request";


export default {

    getUserInfo(userId) {
        return request({
            url: `/api/user/queryUserInfo/`,
            method: 'post',
            params: {
                userId:userId
            },
            data: null // 以json格式传入后端（后端对此参数的要求就是json数据
        })
    },
    //保存
    updateUserInfo(user){
        return request({
            url: `/api/user/updateUserInfo/`,
            method: 'post',
            params: {

            },
            data: user // 以json格式传入后端（后端对此参数的要求就是json数据
        })
    },
    //注销账户
    cancelAccount(userId,password){
         return request({
             url:'/api/user/cancelAccount',
             method:'post',
             params:{
                 userId:userId,
                 password:password
             },
             data:null
         })
    },
    updatePassword(userId,oldPassword,newPassword){
        return request({
            url:'/api/user/updatePassword',
            method:'post',
            params:{
                userId:userId,
                oldPassword:oldPassword,
                newPassword:newPassword
            },
            data:null
        })
    },
    checkAccount(account){
        return request({
            url:'/api/user/checkAccount',
            method:'post',
            params:{
                account:account
            },
            data:null
        })
    },
    checkAccountInfo(account,userId){
        return request({
            url:'/api/user/checkAccountInfo',
            method:'post',
            params:{
                account:account,
                userId:userId
            },
            data:null
        })
    }

    ,
    addAudit(apply){
        return request({
            url: '/api/auditManage/addAudit',
            method: 'post',
            data: apply,
        })
    },
    findAuditByUserId(userId){
        return request({
            url: '/api/auditManage/findAuditUserId',
            method: 'get',
            params: {
                userId:userId
            },
        })
    },
    getUserHistory(currentPage, pageSize, reportQuery) {
        return request({
            url: `/api/query/userHistoryInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: reportQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）
        })
    },
}
