import request from '@/utils/request'
// request中对axios进行了封装


export default {
    getGroupInfo(userId) {
        return request({
            url: `/api/group/listGroups/`,
            method: 'get',
            params: {
                userId:userId
            }
        })
    },
    addGroup(group) {
        return request({
            url: '/api/group/insert/',
            method: 'post',
            data: group
        })
    },
    deleteGroup(groupId) {
        return request({
            url: '/api/group/delete/',
            method: 'get',
            params: {
                groupId: groupId
            }
        })
    }
}