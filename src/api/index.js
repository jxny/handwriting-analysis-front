import request from '../utils/request';
import data2blob from "vue-image-crop-upload/utils/data2blob";

export function login(username, password) {
    return request({
        url: '/user/login',
        method: 'post',
        data: {
            username: username,
            password: password
        }
    });
}

export function register(username, password, email) {
    return request({
        url: '/api/user/register',
        method: 'post',
        data: {
            username: username,
            password: password,
            email: email
        }
    });
}

export function sendEmail(email) {
    return request({
        url: '/api/user/sendEmail',
        method: 'get',
        params: {
            email: email
        }
    });


}

export default {
    getInfo() {
        return request({
            url: '/api/super/getSysInfo',
            method: 'get'
        });
    },
    getUserInfo(currentPage, pageSize, userQuery) {
        return request({
            url: `/api/query/userInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: userQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）

        })
    },
    getRole(currentPage, pageSize, Query) {
        return request({
            url: `/api/query/roleInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: Query  // 以json格式传入后端（后端对此参数的要求就是json数据）

        })
    },
    getRight(currentPage, pageSize, Query) {
        return request({
            url: `/api/query/rightInfo`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: Query  // 以json格式传入后端（后端对此参数的要求就是json数据）

        })
    },
    findRoleByUserId(id) {
        return request({
            url: `/api/user/findById/`,
            method: 'post',
            params: {
                id
            }
        })
    },
    findRightByRoleId(id) {
        return request({
            url: `/api/role/findById/`,
            method: 'post',
            params: {
                id
            }
        })
    },
    addRoleToUser(userId, roleId) {
        return request({
            url: `/api/user/insertRole/`,
            method: 'post',
            params: {
                userId
            },
            data: {
                "array": roleId
            }
        })
    },
    addRightToRole(rightId, roleId) {
        return request({
            url: `/api/role/insertRight/`,
            method: 'post',
            params: {
                roleId
            },
            data: {
                "array": rightId
            }
        })
    },
    deleteRoleToUser(userId, roleId) {
        return request({
            url: `/api/user/deleteRole/`,
            method: 'post',
            params: {
                userId
            },
            data: {
                "array": roleId
            }
        })
    },
    deleteRightToRole(roleId, rightId) {
        return request({
            url: `/api/role/deleteRight/`,
            method: 'post',
            params: {
                roleId
            },
            data: {
                "array": rightId
            }
        })
    },
    deletePermission(array) {
        return request({
            url: `/api/right/delete/`,
            method: 'post',
            data: {
                array
            }
        })
    },
    deleteLog(array) {
        return request({
            url: `/api/log/deleteSome/`,
            method: 'post',
            data: {array}
        })
    },
    addRight(right) {
        return request({
            url: `/api/right/insert/`,
            method: 'post',
            data: right
        })
    },
    addRole(role) {
        return request({
            url: `/api/role/insert/`,
            method: 'post',
            data: role
        })
    },
    prohibit(id, status, type) {
        return request({
            url: `/api/right/updateStatusById/`,
            method: 'post',
            params: {
                id, status, type
            }
        })
    },
    getOperLog(currentPage, pageSize, Query) {
        return request({
            url: '/api/query/logInfo',
            method: 'post',
            params: {
                currentPage,
                pageSize
            },
            data: Query
        });
    },
    /* ========================心理分析=============================================================*/
    //获取预处理图片
    getpretreatmentImges(data,preId) {
        let fmData = new FormData()
        fmData.append("file", data2blob(data, "image/png"),)
        return request({
            url: '/api/analyze/preHandle',
            method: 'post',
            responseType: "blob",
            data: fmData,
            params: {
                preId: preId
            },
        });
    },
    //保存图片,example/testFiles这个接口已经测试可以拿到数据，这个request要求返回一个reprotId
    submitAnalysisImages(originalUrlList,pretreatmentUrlList,n,id,type,date,preId){
        let fmData = new FormData()
        for (let i = 0; i < n; i++) {
            fmData.append("file0"+i+".png", data2blob(originalUrlList[i].url, "image/png"),)
            fmData.append("file1"+i+".png", data2blob(pretreatmentUrlList[i].url, "image/png"),)
        }
        return request({
            url: '/api/handwritingImg/saveImages',
            method: 'post',
            data: fmData,
            params: {
                id: id,
                type: type,
                date: date,
                preId:preId,
            },
        });
    },
    //真正的分析
    analysisReport(pretreatmentUrlList,n,reportId,anaId){
        let fmData = new FormData()
        for (let i = 0; i < n; i++) {
            fmData.append("file"+i+".png", data2blob(pretreatmentUrlList[i].url, "image/png"),)
        }
        return request({
            url: '/api/report/analyze',
            method: 'post',
            data: fmData,
            params: {
                reportId: reportId,
                anaId: anaId,
            },
        });
    },
    //查询所有预处理算法
    getPreAlgorithm(){
        return request({
            url: '/api/algManage/preAlg',
            method: 'post',
        });
    },
    //查询所有分析算法
    getAnaAlgorithm(){
        return request({
            url: '/api/algManage/anaAlg',
            method: 'post',
        });
    },
    //查询默认算法
    getDefaultAlgorithm(){
        return request({
            url: '/api/algManage/defaultAlg',
            method: 'post',
        });
    },
    //根据reportId获取原图片和预处理图片
    getNotAnalysisData(array){
        return request({
            url: '/api/handwritingImg/getImages',
            method: 'post',
            data: {array}
        });
    },
    /* ========================分析报告=============================================================*/
    //根据reportId下载report，后端要求返回流文件.zip压缩包，页面会直接下载
    downReport(array){
        return request({
            url: '/api/report/getReportBatch',
            method: 'post',
            responseType: 'blob',
            data: {array}
        });
    },
    //根据reportId再次分析性格报告
    repeatAnalysis(array){
        return request({
            url: '/api/',
            method: 'post',
            data: {array}
        });
    },
    //根据reportId查找分析报告数据
    getReprot(id){
        return request({
            url: '/api/',
            method: 'post',
            params: {
                id: id
            },
        });
    },
    /* ========================数据备份=============================================================*/
    delBackUp(array) {
        return request({
            url: `/api/backup/delete`,
            method: 'post',
            data: {
                array
            }
        })
    },
    getBackUpData(currentPage, pageSize, Query) {
        return request({
            url: `/api/query/backupInfo`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: Query
        })
    },
    getTime() {
        return request({
            url: `/api/backup/getTime`,
            method: 'get',
        })
    },
    backUpRemote(fileName) {
        return request({
            url: `/api/backup/hand`,
            method: 'get',
            params: {
                fileName: fileName
            },
        })
    },
    backUpLocal(path) {
        return request({
            url: `/api/backup/handToLocal`,
            method: 'get',
            params: {
                path: path
            },
        })
    },
    autoBackUp(time) {
        return request({
            url: `/api/backup/auto`,
            method: 'get',
            params: {
                time: time
            },
        })
    },
    recover(id) {
        return request({
            url: `/api/backup/recover`,
            method: 'get',
            params: {
                id: id
            },
        })
    },
    /* ========================其它=============================================================*/
}
