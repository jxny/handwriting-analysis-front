import request from '@/utils/request'
// request中对axios进行了封装

/*多个页面公用的 api 接口*/
export default {
    /* api 前面的’/‘ 一定要写，否者请求路径不正确，因为'/' 相当于根路径，否者只会在原有路径上替换一部分路径，请求路径就不正确了*/
    userLogin(login) {
        return request({
            url: '/api/user/login',
            method: 'post',
            data: login
        })
    },
    // 发送注册验证码
    sendRegisterCode(email) {
        return request({
            url: '/api/user/sendRegisterCode',
            method: 'post',
            params: {
                email
            }
        })
    },
    // 重置密码
    resetPassword(email, code, newPassword) {
        return request({
            url: '/api/user/forgetPassword',
            method: 'post',
            params: {
                email: email,
                code: code,
                newPassword: newPassword
            }
        })
    },
    // 注册
    register(user, code) {
        return request({
            url: '/api/user/register',
            method: 'post',
            params: {
                code
            },
            data: user
        })
    },
    // 检查邮箱是否存在
    checkEmail(email) {
        return request({
            url: '/api/user/checkEmail',
            method: 'get',
            params: {
                email
            }
        })
    },
    // 更新密码
    forgetPassword(email, newPassword, code) {
        return request({
            url: '/api/user/forgetPassword',
            method: 'post',
            params: {
                email,
                newPassword,
                code,
            }
        })
    },
    // 发送 忘记密码 验证码
    sendForgetPasswordCode(email) {
        return request({
            url: '/api/user/sendForgetPasswordCode',
            method: 'get',
            params: {
                email
            }
        })
    }

}