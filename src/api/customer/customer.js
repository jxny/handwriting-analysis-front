import request from '@/utils/request'
// request中对axios进行了封装


export default {
    getCustomerInfo(userId,currentPage, pageSize, customerQuery) {
        return request({
            url: `/api/query/customerInfo/`,
            method: 'post',
            params: {
                userId:userId,
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: customerQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）
        })
    },
    deleteCustomerBatch(array){
        return request({
            url: '/api/customer/delete',
            method: 'post',
            data: array,
        })
    },
    moveCustomer(groupId,array) {
        return request({
            url: '/api/customer/move/',
            method: 'post',
            params: {
                groupId: groupId,
            },
            data: array,
        })
    },
    addCustomer(customer) {
        return request({
            url: '/api/customer/insert/',
            method: 'post',
            data: customer,
        })
    },
    getCustomerInformation(id) {
        return request({
            url: '/api/customer/findById/',
            method: 'get',
            params: {
                id: id
            }
        })
    },
    updateCustomer(customer) {
        return request({
            url: '/api/customer/update/',
            method: 'post',
            data: customer,
        })
    },

    // 获取客户的所有历史记录
    getHistory(customerId,status) {
        return request({
            url: '/api/customer/history',
            method: 'get',
            params: {
                customerId: customerId,
                status: status
            }
        })
    },

    // 获取所有的未分析客户
    getNotAnaCustomer(currentPage, pageSize, customerQuery) {
        return request({
            url: '/api/query/customerIsAnalysis',
            method: 'post',
            data: customerQuery,
            params: {
                currentPage: currentPage,
                pageSize: pageSize,
            }
        })
    },

    // 获取客户的所有未分析记录
    getNotAnaRecord(customerId,status) {
        return request({
            url: '/api/customer/history',
            method: 'get',
            params: {
                customerId: customerId,
                status: status
            }
        })
    },

    // 批量分析未分析客户
    batchAnaly(array,selectedAnaId) {
        return request({
            url: '/api/report/analyzeBatchNoAnalysis',
            method: 'post',
            params: {
                anaId: selectedAnaId,
            },
            data: array
        })
    },
    // 批量分析未分析记录
    batchAnalyRecord(array,selectAnaId) {
        return request({
            url: '/api/report/analyzeBatch',
            method: 'post',
            params: {
                anaId: selectAnaId,
            },
            data: array
        })
    },
    // 批量删除记录
    deleteRecordBatch(array) {
        return request({
            url: '/api/report/deleteReportById',
            method: 'post',
            data: array
        })
    }

}