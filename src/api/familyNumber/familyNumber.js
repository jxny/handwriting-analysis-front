import request from '@/utils/request'
// request中对axios进行了封装

export default {
    addFamilyNumbers(familyNumberList, customerId) {
        return request({
            url: '/api/familyNumber/insert/',
            method: 'post',
            params: {
                "customerId": customerId,
            },
            data: {
                "array":familyNumberList,
            }
        })
    },
    updateFamilyNumbers(familyNumberList) {
        return request({
            url: '/api/familyNumber/update/',
            method: 'post',
            data: {
                "array": familyNumberList,
            }
        })
    }
}