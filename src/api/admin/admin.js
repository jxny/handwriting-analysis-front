import request from '@/utils/request'
// request中对axios进行了封装


export default {
    /* api 前面的’/‘ 一定要写，否者请求路径不正确，因为'/' 相当于根路径，否者只会在原有路径上替换一部分路径，请求路径就不正确了*/

    getUserInfo(currentPage, pageSize, userQuery) {
        return request({
            url: `/api/query/userInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: userQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）

        })
    },


    /*================================================================算法url*/
    getAlgorithm(currentPage, pageSize, algQuery) {
        return request({
            url: `/api/query/algorithmInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: algQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）
        })
    },
    searchAlgInfo(currentPage, pageSize, algorithmQuery) {
        return request({
            url: `/api/query/algorithmInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: algorithmQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）
        })
    },
    getAllAnaAlg() {
        return request({
            url: `/api/algManage/anaAlg/`,
            method: 'get',
        })
    },
    getAllPreAlg() {
        return request({
            url: `/api/algManage/preAlg/`,
            method: 'get',
        })
    },
    deleteAlgBatch(array) {
        return request({
            url: `/api/algManage/deleteAlg/`,
            method: 'post',
            data: array,
        })
    },
    // 获取某一个算法的详细信息
    getAlgDetail(algId) {
        return request({
            url: '/api/algManage/findAlgView',
            method: 'get',
            params: {
                algId: algId,
            }
        })
    },
    switchAlgorithm(changeAlg){
        return request({
            url: '/api/algManage/switchAlg',
            method: 'get',
            params: {
                preId: changeAlg.radioPre,
                anaId: changeAlg.radioAna
            }
        })
    },
    addAlg(algorithm){
        return request({
            url: '/api/algManage/addAlg',
            method: 'post',
            data: algorithm,
        })
    },

    /*================================================================*/

    /*================================================================特征url*/
    getFeature(currentPage, pageSize, featureQuery) {
        return request({
            url: `/api/query/featureInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: featureQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）

        })
    },
    searchFeaInfo(currentPage, pageSize, featureQuery) {
        return request({
            url: `/api/query/featureInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: featureQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）
        })
    },
    prohibitFeature(id, status) {
        return request({
            url: '/api/featureManage/activateFeature',
            method: 'get',
            params: {
                featureId: id,
                featureStatus: status,
            }
        })
    },
    /*================================================================*/

    /*================================================================申请url*/
    getAudit(currentPage, pageSize, auditQuery) {
        return request({
            url: `/api/query/auditInfo/`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: auditQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）

        })
    },
    searchApplyInfo(currentPage, pageSize, applyQuery) {
        return request({
            url: `/api/query/auditInfo`,
            method: 'post',
            params: {
                currentPage: currentPage,
                pageSize: pageSize
            },
            data: applyQuery  // 以json格式传入后端（后端对此参数的要求就是json数据）
        })
    },
    deleteApplyBatch(array) {
        return request({
            url: `/api/auditManage/deleteAudit`,
            method: 'post',
            data: array,
        })
    },
    // 获取某一个申请的详细信息
    getApplyDetail(auditId) {
        return request({
            url: '/api/auditManage/findAuditView',
            method: 'get',
            params: {
                auditId: auditId,
            }
        })
    },
    auditApply(audit){
        return request({
            url: '/api/auditManage/doAudit',
            method: 'get',
            params: {
                id: audit.id,
                userId: audit.userId,
                auditId: audit.auditId,
                result: audit.auditResult,
                auditDes: audit.auditDes,
            }
        })
    },
    /*================================================================*/

    // 获取某一个用户的详细个人信息
    getUserDetail(userId) {
        return request({
            url: '/api/user/findUser',
            method: 'get',
            params: {
                userId: userId,
            }
        })

    },
    deleteUserById(id) {
        return request({
            url: '/api/user/deleteUser',
            method: 'get',
            params: {
                id: id
            }
        })
    },
    deleteUserBatch(array) {
        return request({
            url: '/api/user/deleteUser',
            method: 'post',
            data: array,
        })
    },
    exportUserInfo(array) {
        return request({
            url: '/api/export/userInfo',
            method: 'post',
            responseType: 'blob',
            data: array,
        })
    },
    prohibitUser(id, status) {
        return request({
            url: '/api/user/prohibitUser',
            method: 'get',
            params: {
                id: id,
                status: status,
            }
        })
    },
    insertUser(user) {
        return request({
            url: '/api/user/insertUser',
            method: 'post',
            data: user,
        })
    }
}