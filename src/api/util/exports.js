import request from '@/utils/request'
// request中对axios进行了封装

export default {
    exportCustomerInfo(array) {
        return request({
            url: '/api/export/customer/',
            method: 'post',
            data: array,
            responseType: 'blob'
        })
    }
}