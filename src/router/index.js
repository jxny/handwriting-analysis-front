import {createRouter, createWebHistory} from "vue-router";
import Home from "../views/Home.vue";
import Admin from "@/views/Admin"
import Super from "../views/super/Super"
import UserIn from "../views/user/UserIn";

const routes = [
    {
        path: '/',
        redirect: '/login'
    },

    {
        path: '/index',
        name: 'Index',
        meta: {
            title: '介绍页---笔迹性格分析系统'
        },
        component: () => import("@/views/Index")
    },
    {
        path: "/login",
        name: "UserLogin",
        meta: {
            title: '用户登录'
        },
        component: () => import (
            "@/views/user/UserLogin")
    },
    {
        path: "/register",
        name: "Register",
        meta: {
            title: '用户注册'
        },
        component: () => import (
            "@/views/user/UserRegister")
    },
    {
        path: "/forget",
        name: "Forget",
        meta: {
            title: '忘记密码'
        },
        component: () => import (
            "@/views/user/forget/ForgetPassword"),
    },
    {
        path: "/forget/reset",
        name: "ResetPassword",
        meta: {
            title: '重置密码'
        },
        component: () => import (
            "@/views/user/forget/ForgetPassword1")
    },
    {
        path: "/forget/success",
        name: "ResetSuccess",
        meta: {
            title: '重置成功'
        },
        component: () => import (
            /* webpackChunkName: "register" */
            "../views/user/forget/ForgetPassword2")
    }
    ,
    {
        path: "/userTest",
        name: "user",
        meta: {
            title: '用户管理'
        },
        component: () => import(
            "../views/super/power/User"
            )
    },
    //         "@/views/user/forget/ForgetPassword2")
    // },
    {
        path: "/managerLogin",
        name: "managerLogin",
        meta: {
            title: '管理员登录'
        },
        component: () => import (
            /* webpackChunkName: "login" */
            "../views/ManagerLogin.vue")
    },

    // {
    //     path: "/login",
    //     name: "Login",
    //     meta: {
    //         title: '登录'
    //     },
    //     component: () => import (
    //         /* webpackChunkName: "login" */
    //         "../views/Login.vue")
    // },
    // {
    //     path: "/register",
    //     name: "Register",
    //     meta: {
    //         title: '注册'
    //     },
    //     component: () => import (
    //         /* webpackChunkName: "register" */
    //         "../views/Register.vue")
    // },


    //user路由
    {
        path: "/home",
        name: "Home",
        component: () => import (
            "@/views/user/Home.vue"),
        children: [
            {
                path: "analysis",
                name: "Analysis",
                meta: {
                    title: '笔迹分析'
                },
                component: () => import (
                    "@/views/Analysis")
            }, {
                path: "psy",
                name: "Psy",
                meta: {
                    title: '笔迹分析'
                },
                component: () => import (
                    "@/views/PsyAnalysis.vue")
            },{
                path: "analysisReport",
                name: "AnalysisReport",
                meta: {
                    title: '笔迹分析报告'
                },
                component: () => import (
                    "@/views/AnalysisReport.vue")
            },
            {
                path: "dataAnalysis",
                name: "DataAnalysis",
                meta: {
                    title: '数据分析'
                },
                component: () => import (
                    "@/views/DataAnalysis.vue")
            },{
                path: "batchAnalysis",
                name: "BatchAnalysis",
                meta: {
                    title: '数据分析'
                },
                component: () => import (
                    "@/views/BatchAnalysis.vue")
            },{
                path: "index",
                name: "UserIndex",
                meta: {
                    title: "首页-笔迹性格分析系统"
                },
                component: () => import ("@/views/user/UserIndex")
            },{
                path: "apply",
                name: "ApplyPsychiatrist",
                meta: {
                    title: "申请心理咨询师"
                },
                component: () => import ("@/views/user/applyPsychiatrist")
            },
            {
                path: "history",
                name: "history",
                meta: {
                    title: "用户历史"
                },
                component: () => import ("@/views/user/userHistory")
            }
        ]
    },

    {
        path: "/userIn",
        name: "UserIn",
        component:UserIn,
        children: [
            {
                path: "userInfo",
                name: "userInfo",
                meta: {
                    title: '我的信息'
                },
                component: () => import (
                    "@/views/user/userInfo.vue")
            },
            {
                path: "cancelAccount",
                name: "cancelAccount",
                meta: {
                    title: '注销账号'
                },
                component: () => import (
                    "@/views/user/cancelAccount.vue")
            },
            {
                path: "resetPassword",
                name: "resetPassword",
                meta: {
                    title: '重置密码'
                },
                component: () => import (
                    "@/views/user/resetPassword.vue")
            }
        ]
    },

    // 超级管理员路由
    {
        path: "/super",
        name: "Super",
        redirect: '/super/operLogTable',
        component: Super,
        children: [
            {
                path: "user",
                name: "user",
                meta: {
                    title: '用户'
                },
                component: () => import (
                    /* webpackChunkName: "table" */
                    "../views/super/power/User.vue")
            },
            {
                path: "role",
                name: "role",
                meta: {
                    title: '角色'
                },
                component: () => import (
                    /* webpackChunkName: "table" */
                    "../views/super/power/Role.vue")
            },
            {
                path: "permission",
                name: "permission",
                meta: {
                    title: '权限'
                },
                component: () => import (
                    /* webpackChunkName: "table" */
                    "../views/super/power/Permission.vue")
            },
            {
                path: "sysInfo",
                name: "sysInfo",
                meta: {
                    title: '系统监控'
                },
                component: () => import (
                    /* webpackChunkName: "charts" */
                    "../views/super/sysInfo/SysInfo.vue")
            }, {
                path: 'operLogTable',
                name: 'operLogTable',
                meta: {
                    title: '操作日志'
                },
                component: () => import (/* webpackChunkName: "operLogTable" */
                    '../views/super/log/OperLog.vue')
            }, {
                path: 'backUp',
                name: 'BackUp',
                meta: {
                    title: '数据备份'
                },
                component: () => import (/* webpackChunkName: "operLogTable" */
                    '../views/super/backup/BackUp')
            }
        ]
    },



    /*管理员路由*/
    {
        path: "/admin",
        name: "Admin",
        redirect: '/admin/user',
        component: Admin,
        children: [
            {
                path: "dashboard",
                name: "Dashboard",
                meta: {
                    title: '系统首页'
                },
                component: () => import (
                    /* webpackChunkName: "dashboard" */
                    "../views/mode/Dashboard.vue")
            },{
                    path: "user",
                    name: "User",
                    meta: {
                        title: '用户管理'
                    },
                    component: () => import (
                    "@/views/admin/userManager.vue")
            },{
                path: "algorithm",
                name: "Algorithm",
                meta: {
                    title: '算法管理'
                },
                component: () => import (
                    "@/views/admin/algorithmManager.vue")
            },{
                path: "feature",
                name: "Feature",
                meta: {
                    title: '特征管理'
                },
                component: () => import (
                    "@/views/admin/featureManager.vue")
            },{
                path: "apply",
                name: "Apply",
                meta: {
                    title: '申请管理'
                },
                component: () => import (
                    "@/views/admin/applyManager.vue")
            },{
                path: "userDetail",
                name: "UserDetail",
                meta: {
                    title: '用户详情',
                    noCache: true
                },
                component: () => import (
                    "@/views/admin/addOrViewUser")
            },{
                path: "addUser",
                name: "AddUser",
                meta: {
                    title: '添加用户',
                    noCache: true
                },
                component: () => import (
                    "@/views/admin/addUser")
            },{
                path: "addAlgorithm",
                name: "AddAlgorithm",
                meta: {
                    title: '添加算法',
                    noCache: true
                },
                component: () => import (
                    "@/views/admin/addAlgorithm")
            },{
                path: "viewAlgorithm",
                name: "ViewAlgorithm",
                meta: {
                    title: '算法详情',
                    noCache: true
                },
                component: () => import (
                    "@/views/admin/viewAlgorithm")
            },{
                path: "viewApply",
                name: "ViewApply",
                meta: {
                    title: '申请详情',
                    noCache: true
                },
                component: () => import (
                    "@/views/admin/viewApply")
            },


            ]
    },

    // 心理咨询师路由
    {
        path: "/customer",
        name: "Customer",
        component: () => import (
            "@/views/customer/CustomerHome.vue"),
        children: [
          {
                path: "list",
                name: "CustomerManager",
                meta: {
                    title: '客户管理'
                },
                component: () => import(
                    "@/views/customer/CustomerManager"
                    )
            },
            {
                path: "show",
                name: "CustomerShow",
                meta: {
                    title: '主页'
                },
                component: () => import(
                    "@/views/customer/Main"
                    )
            },
            {
                path: "add",
                name: "AddCustomer",
                meta: {
                    title: '录入客户信息'
                },
                component: () => import(
                    "@/views/customer/AddCustomer"
                    )
            },
            {
                path: "info",
                name: "CustomerInfo",
                meta: {
                    title: '客户详细信息'
                },
                component: () => import(
                    "@/views/customer/ViewCustomer"
                    )
            },
            {
                path: "notAnalysis",
                name: "NotAnalyRecord",
                meta: {
                    title: '未分析记录',
                },
                component: () => import(
                    "@/views/customer/NotAnalyRecord"
                    )
            },
            {
                path: "history",
                name: "HistoryRecord",
                meta: {
                    title: '历史记录',
                },
                component: () => import(
                    "@/views/customer/HistoryRecord"
                    )
            },
        ],
    },



    /*所有的不匹配路由都会跳转到404页面*/
    {
        path: '/404',
        name: 'NotFound',
        component: () => import("@/views/mode/404"),
        meta: {
            title: 'Page not found',
            isLogin: false
        },
    },
    {
        path: '/:pathMatch(.*)',
        redirect: '/404',
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

/*router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | vue-template`;
    const role = localStorage.getItem('ms_username');
    if (!role && to.path !== '/login') {
        next('/login');
    } else if (to.meta.permission) {
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        role === 'admin'
            ? next()
            : next('/403');
    } else {
        next();
    }
});*/

router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title}`;
    next();
});
// const baseRoutes = [];
// const allroute = baseRoutes.concat(router, supper);
//
// export default new VueRouter({
//     allroute,
// });
export default router;

